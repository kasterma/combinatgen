(ns combinatGen.core)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;               Enumerating Sequences
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




; initial digits all zero
(def initdig (seq (repeat 7 0)))
; choice of radices for the example computations
(def radices (list  9 9 9 2 3 2 4))

(defn succ
  ([cur] (succ cur radices))
  ([cur radices]
     (if (> (count cur) 0)
       (if (< (first cur) (- (first radices) 1))
         (cons (+ (first cur) 1) (rest cur))
         (let [tail (succ (rest cur) (rest radices))]
           (if (= tail nil)
             nil
             (cons 0 tail))))
       nil)))

(defn n-succ [n cur]
  (if (= n 0)
    cur
    (n-succ (- n 1) (succ cur))))

(succ initdig radices)

(def ninth-elt (n-succ 9 initdig))

(def all (iterate succ initdig))

(def first10 (take 10 all))

(def complete (take-while #(> (count %) 0) all))

(defn visitall
  "visit all elements in the sequence"
  ([visit] (visitall visit initdig))
  ([visit cur]
     (do (visit cur)
         (let [next (succ cur)]
           (if (= next nil)
             nil
             (visitall visit next))))))

(defn visitall-r
  "recursive version of visitall"
  ([visit] (visitall-r visit initdig))
  ([visit cur]
     (do (visit cur)
         (let [next (succ cur)]
           (if (= next nil)
             nil
             (recur visit next))))))

(visitall-r #(println "visiting: " %))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;               Computing Grey codes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn plusmod2 [x y]
  (rem (+ x y) 2))

(defn binary2gray [[b0 b1 & remainder :as wholeseq]]
  "from binary representation of a number generate the grey code"
  (if (= b1 nil)
    (list b0)
    (cons (plusmod2 b0 b1) (binary2gray (rest wholeseq)))))

(defn bin2gray2 [wholeseq]
  (let [[b0 & tail] wholeseq
        b1 (first tail)]
    (if (= b1 nil)
      (list b0)
      (cons (plusmod2 b0 b1) (binary2gray tail)))))

    
(def initdig (repeat 4 0))
(def radices (repeat 4 2))
(def all (iterate #(succ % radices) initdig))
(def complete (take-while #(> (count %) 0) all))
(def gray4 (map binary2gray complete))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;               Generating Permutations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Implementing Algorithm L, staying close to the presentation in the book
;; which is why this is clearly insufficiently commented to be readable
;; also lists are clearly not the right datastructure to use, so the below
;; is not very efficient.  Presumably just changing to vectors will make
;; a big difference.

(def initial (take 11 (iterate inc 0)))

(defn findj [in_perm]
  (loop [j (- (count in_perm) 2)]
    (if (>= (nth in_perm j) (nth in_perm (+ j 1)))
      (recur (- j 1))
              j)))

(defn findl [in_perm j]
  (let [aj (nth in_perm j)]
    (loop [i (- (count in_perm) 1)]
      (if (>= aj (nth in_perm i))
        (recur (- i 1))
        i))))

(defn flip [inperm i j]
  (if (< i j)
    (let [ai (nth inperm i)
          aj (nth inperm j)]
      (concat (take i inperm)
              (list aj)
              (take (- j (+ i 1)) (drop (+ i 1) inperm))
              (list ai)
              (drop (+ j 1) inperm)))
    (flip inperm j i)))

(defn reversePart [in_perm k l]
    (if (< k l)
      (flip (reversePart in_perm (+ k 1) (- l 1)) k l)
      in_perm)) 

(defn nextperm [in_perm]
  (let [j (findj in_perm)]
    (if (== j 0)
      nil
      (let [l (findl in_perm j)
            interm_perm (flip in_perm j l)
            next_perm (reversePart interm_perm (+ j 1) (- (count in_perm) 1))]
        next_perm))))
    

(def initialBook (list 1 2 2 3))

(nextperm initialBook)

(def allPerm
  (map rest
       (take-while #(not= % nil)
                   (iterate nextperm (cons 0 initialBook)))))